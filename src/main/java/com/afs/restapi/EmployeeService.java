package com.afs.restapi;

import com.afs.restapi.exception.InactiveEmployeeException;
import com.afs.restapi.exception.InvalidEmployeeException;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class EmployeeService {

    @Autowired
    EmployeeRepository employeeRepository;

    public Employee insert(Employee employee) {
        if (employee.getAge() < 18 || employee.getAge() > 65) {
            throw new InvalidEmployeeException();
        }
        return employeeRepository.insert(employee);
    }

    public List<Employee> findAll() {
        return employeeRepository.findAll();

    }

    public Employee findById(Long id) {
        return employeeRepository.findById(id);
    }

    public List<Employee> findByGender(String gender) {
        return employeeRepository.findByGender(gender);
    }

    public List<Employee> findByPage(Integer pageNumber, Integer pageSize) {
        return employeeRepository.findByPage(pageNumber, pageSize);
    }

    public Employee update(Long id, Employee employee) {
        Employee targetEmployee = employeeRepository.findById(id);
        if (!targetEmployee.isActiveStatus()) {
            throw new InactiveEmployeeException();
        }

        return employeeRepository.update(id, employee);
    }

    public void delete(Long id) {
        employeeRepository.delete(id);
    }
}
