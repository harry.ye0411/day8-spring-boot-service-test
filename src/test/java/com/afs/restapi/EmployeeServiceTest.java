package com.afs.restapi;

import com.afs.restapi.exception.InactiveEmployeeException;
import com.afs.restapi.exception.InvalidEmployeeException;
import org.junit.jupiter.api.Test;
import org.junit.jupiter.api.extension.ExtendWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit.jupiter.SpringExtension;

import static org.junit.jupiter.api.Assertions.assertThrows;
import static org.mockito.Mockito.*;

@ExtendWith(SpringExtension.class)
public class EmployeeServiceTest {

    @InjectMocks
    EmployeeService employeeService;

    @Mock
    private EmployeeRepository employeeRepository;

    @Test
    void should_throw_invalid_employee_exception_when_given_employee_too_young() {
        Employee employee = new Employee(6L, "John Smith", 15, "Male", 5000);

        assertThrows(InvalidEmployeeException.class, () -> employeeService.insert(employee));
    }

    @Test
    void should_throw_invalid_employee_exception_when_given_employee_too_old() {
        Employee employee = new Employee(6L, "John Smith", 69, "Male", 5000);

        assertThrows(InvalidEmployeeException.class, () -> employeeService.insert(employee));
    }

    @Test
    void should_create_employee_when_given_employee_has_valid_age() {
        Employee employee = new Employee(6L, "John Smith", 32, "Male", 5000);

        employeeService.insert(employee);

        verify(employeeRepository, times(1)).insert(employee);
    }

    @Test
    void should_return_all_employees_when_calling_findAll() {
        employeeService.findAll();

        verify(employeeRepository, times(1)).findAll();
    }

    @Test
    void should_return_correct_employee_when_calling_findById_with_existing_id() {
        Long id = 1L;

        employeeService.findById(id);

        verify(employeeRepository, times(1)).findById(id);
    }

    @Test
    void should_return_employees_with_given_gender_when_calling_findByGender() {
        String gender = "Male";

        employeeService.findByGender(gender);

        verify(employeeRepository, times(1)).findByGender(gender);
    }

    @Test
    void should_return_employees_subset_when_calling_findByPage() {
        Integer pageNumber = 1;
        Integer pageSize = 10;

        employeeService.findByPage(pageNumber, pageSize);

        verify(employeeRepository, times(1)).findByPage(pageNumber, pageSize);
    }

    @Test
    void should_throw_inactive_employee_exception_when_updating_an_inactive_employee() {
        Long id = 1L;
        Employee employee = new Employee(id, "John Smith", 32, "Male", 5000);
        employee.setActiveStatus(false);

        when(employeeRepository.findById(id)).thenReturn(employee);

        assertThrows(InactiveEmployeeException.class, () -> employeeService.update(id, employee));
    }

    @Test
    void should_update_employee_when_updating_an_active_employee() {
        Long id = 1L;
        Employee employee = new Employee(id, "John Smith", 32, "Male", 5000);
        Employee employeeToUpdate = new Employee(id, "John Smith", 33, "Male", 6000);

        when(employeeRepository.findById(id)).thenReturn(employeeToUpdate);

        employeeService.update(id, employee);

        verify(employeeRepository, times(1)).update(id, employee);
    }

    @Test
    void should_delete_employee_when_given_a_valid_id() {
        Long id = 1L;

        employeeService.delete(id);

        verify(employeeRepository, times(1)).delete(id);
    }
}