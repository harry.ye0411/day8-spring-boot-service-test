package com.afs.restapi;

import com.fasterxml.jackson.databind.ObjectMapper;
import org.junit.jupiter.api.BeforeEach;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.http.MediaType;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders;

import static org.hamcrest.Matchers.hasSize;
import static org.junit.jupiter.api.Assertions.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.jsonPath;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

@SpringBootTest
@AutoConfigureMockMvc
public class EmployeeControllerTest {

    @Autowired
    MockMvc mockClientSide;

    @Autowired
    ObjectMapper objectMapper;

    @Autowired
    EmployeeRepository employeeRepository;

    @BeforeEach
    void setUp() {
        employeeRepository.reset();
    }

    @Test
    void should_return_all_employees_when_calling_getAllEmployees() throws Exception {
        mockClientSide.perform(MockMvcRequestBuilders.get("/employees"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(5)));
    }

    @Test
    void should_return_correct_employee_when_calling_getEmployeesById() throws Exception {
        Long id = 1L;
        String expect = objectMapper.writeValueAsString(
                new Employee(id, "John Smith", 32, "Male", 5000)
        );

        mockClientSide.perform(MockMvcRequestBuilders.get("/employees/" + id))
                .andExpect(status().isOk())
                .andExpect(result -> assertEquals(expect, result.getResponse().getContentAsString()));
    }

    @Test
    void should_return_3_employees_when_calling_findMaleEmployees() throws Exception {
        mockClientSide.perform(MockMvcRequestBuilders.get("/employees")
                        .param("gender", "Male"))
                .andExpect(status().isOk())
                .andExpect(jsonPath("$", hasSize(3)));
    }

    @Test
    void should_return_2_employees_when_calling_findByPage_given_pageNumber_and_pageSize() throws Exception {
        Integer pageNumber = 1;
        Integer pageSize = 2;

        mockClientSide.perform(MockMvcRequestBuilders.get("/employees")
                        .param("pageNumber", String.valueOf(pageNumber))
                        .param("pageSize", String.valueOf(pageSize)))
                .andExpect(status().isOk())
                .andExpect(result -> jsonPath("$", hasSize(2)));
    }

    @Test
    void should_return_new_employee_after_inserting_a_new_employee() throws Exception {
        Long id = 6L;
        Employee newEmployee = new Employee(id, "Test", 27, "Female", 7000);
        System.out.println(objectMapper.writeValueAsString(newEmployee));

        String expect = objectMapper.writeValueAsString(newEmployee);

        mockClientSide.perform(MockMvcRequestBuilders.post("/employees")
                        .content(objectMapper.writeValueAsString(newEmployee))
                        .contentType(MediaType.APPLICATION_JSON))
                .andExpect(status().isCreated())
                .andExpect(result -> assertNotNull(result.getResponse().getContentAsString()));

        mockClientSide.perform(MockMvcRequestBuilders.get("/employees/" + id))
                .andExpect(status().isOk())
                .andExpect(result -> assertEquals(expect, result.getResponse().getContentAsString()));
    }

    @Test
    void should_update_employee_successfully_after_calling_updateEmployee() throws Exception {
        Long id = 1L;
        Employee employee = new Employee(id, "John Smith", 32, "Male", 9999);

        mockClientSide.perform(MockMvcRequestBuilders.put("/employees/1")
                        .content(objectMapper.writeValueAsString(employee))
                        .contentType("application/json"))
                .andExpect(status().isOk())
                .andExpect(result -> assertNotNull(result.getResponse().getContentAsString()))
                .andExpect(result -> {
                    Employee actualEmployee = objectMapper.readValue(result.getResponse().getContentAsString(), Employee.class);
                    assertEquals(employee.getAge(), actualEmployee.getAge());
                    assertEquals(employee.getSalary(), actualEmployee.getSalary());
                });
    }

    @Test
    void should_deactivate_the_employee_after_deleting_an_employee_with_given_id() throws Exception {
        long id = 1L;
        mockClientSide.perform(MockMvcRequestBuilders.delete("/employees/" + id))
                .andExpect(status().isNoContent());

        mockClientSide.perform(MockMvcRequestBuilders.get("/employees/" + id))
                .andExpect(status().isOk())
                .andExpect(result -> {
                    Employee actualEmployee = objectMapper.readValue(result.getResponse().getContentAsString(), Employee.class);
                    assertFalse(actualEmployee.isActiveStatus());
                });
    }


    @Test
    void should_return_404_when_trying_to_get_an_employee_that_does_not_exist() throws Exception {
        mockClientSide.perform(MockMvcRequestBuilders.get("/employees/6"))
                .andExpect(status().isNotFound());
    }

    @Test
    void should_return_404_when_trying_to_update_an_employee_that_does_not_exist() throws Exception {
        Employee targetEmployee = new Employee(6L, "John Smith", 32, "Male", 9999);

        mockClientSide.perform(MockMvcRequestBuilders.put("/employees/6")
                        .content(objectMapper.writeValueAsString(targetEmployee))
                        .contentType("application/json"))
                .andExpect(status().isNotFound());
    }

    @Test
    void should_return_404_when_deleting_an_employee_that_does_not_exist() throws Exception {
        mockClientSide.perform(MockMvcRequestBuilders.delete("/employees/6"))
                .andExpect(status().isNotFound());
    }
}

